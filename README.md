# Sistema de Gerenciamento de Clientes

Sistema oferece gerenciamento básico de clientes com endereço e contato.


## Instação
Para instalar basta clonar o repositório e depois usar os seguintes comandos:

Instalar dependências :
```bash
composer install
```

Geração da key:


```bash
php artisan key:generate
```

Atualizando banco de dados.

```bash
php artisan migrate
```

Servindo a aplicação.

```bash
php artisan serve
```

## Teste

As funcionalidades podem ser testadas através do comando :

```bash
php artisan test
```


