<?php

namespace Database\Factories;

use App\Models\Cliente;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClienteFactory extends Factory
{
    protected $model = Cliente::class;

    public function definition()
    {
        return [
            'razao_social'  => $this->faker->name(),
            'nome_fantasia' => $this->faker->name(),
            'pessoa_tipo'   => 'FISICA',
            'documento'     => '000.000.000-00',
            'status'        => $this->faker->randomElement(['ATIVO', 'INATIVO', 'PROSPECTO']),
        ];
    }
}
