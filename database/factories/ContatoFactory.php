<?php

namespace Database\Factories;

use App\Models\Cliente;
use App\Models\Contato;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContatoFactory extends Factory
{
    protected $model = Contato::class;
    private const ID_INVALID = '0';

    public function definition()
    {
        return [
            'nome'       => $this->faker->name(),
            'email'      => $this->faker->email(),
            'telefone'   => '(38)99999-9999',
            'funcao'     => $this->faker->jobTitle(),
            'cliente_id' => self::ID_INVALID,
        ];
    }
}
