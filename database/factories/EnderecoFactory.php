<?php

namespace Database\Factories;

use App\Models\Cliente;
use App\Models\Endereco;
use Illuminate\Database\Eloquent\Factories\Factory;

class EnderecoFactory extends Factory
{
    protected $model = Endereco::class;
    private const ID_INVALID = '0';

    public function definition()
    {
        return [
            'estado'      => $this->faker->country(),
            'cidade'      => $this->faker->city(),
            'rua'         => $this->faker->streetName(),
            'bairro'      => $this->faker->streetName(),
            'numero'      => $this->faker->buildingNumber(),
            'complemento' => '',
            'cliente_id'  => self::ID_INVALID,
        ];
    }
}
