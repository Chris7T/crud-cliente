<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Clientes</title>
    <link href="/css/styles.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <h1><span class="badge bg-secondary">Clientes</span></h1>
    <table class="table table-dark">
        <thead>
            <tr>
                <th scope="col">Código</th>
                <th scope="col">Razão Social</th>
                <th scope="col">Nome Fantasia</th>
                <th scope="col">Tipo de Pessoa</th>
                <th scope="col">Status</th>
                <th scope="col">Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($clientes as $cliente)
            <tr>
                <td>

                    {{$cliente->id}}

                </td>
                <td>{{ $cliente->razao_social }}</td>
                <td>{{ $cliente->nome_fantasia }}</td>
                <td>{{ $cliente->pessoa_tipo }}</td>
                <td>{{ $cliente->status }}</td>
                <td class="center">
                    <div>
                        <a type="button" class="btn btn-outline-info delete-cliente" href="{{url('/cliente/'. $cliente->id . '/edit')}}">Detalhar</a>
                        <form class="fake-form" method="POST" action="/cliente/{{$cliente->id}}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-outline-danger">Apagar</button>
                        </form>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="footer">
        <a type='button' href="{{ route('cliente.create') }}" class="btn btn-light">Adicionar</a>
    </div>

</body>

</html>