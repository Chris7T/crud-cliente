<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Clientes</title>
    <link href="/css/styles.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function($) {
            $('#telefone').mask('(99) 99999-9999');
            var CpfCnpjMascara = function(val) {
                    return val.replace(/\D/g, '').length <= 11 ? '000.000.000-009' : '00.000.000/0000-00';
                },
                cpfCnpjpOptions = {
                    onKeyPress: function(val, e, field, options) {
                        field.mask(CpfCnpjMascara.apply({}, arguments), options);
                    }
                };
            $('#documento').mask(CpfCnpjMascara, cpfCnpjpOptions);
        });
    </script>
</head>

<body>
    <div>
        @if ($cliente->id)
        <h1><span class="badge bg-secondary">Atualizar</span></h1>
        @else
        <h1><span class="badge bg-secondary">Cadastro</span></h1>
        @endif
        <form class="formulario" method="POST" action="{{ route($action, ['cliente' => $cliente->id]) }}">
            @csrf
            @if ($cliente->id)
            @method('PUT')
            @endif

            <div>
                <h3>Dados</h3>
                <div class="row">

                    <div class="col">
                        <label class="form-label" for="razao_social">{{ __('Razão Social: ') }}</label>
                        <input class="form-control @error('razao_social') is-invalid @enderror" type="text" id="razao_social" name="razao_social" value="{{ old('razao_social', $cliente->razao_social) }}">

                        @error('razao_social')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col">
                        <label class="form-label" for="nome_fantasia">{{ __('Nome Fantasia: ') }}</label>
                        <input class="form-control @error('razao_social') is-invalid @enderror" type="text" id="nome_fantasia" name="nome_fantasia" value="{{ old('nome_fantasia', $cliente->nome_fantasia) }}">
                        @error('nome_fantasia')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label class="form-label" for="pessoa_tipo">{{ __('Tipo de Pessoa: ') }}</label>
                        <select class="form-select  @error('razao_social') is-invalid @enderror" id="pessoa_tipo" name="pessoa_tipo" value="{{ old('pessoa_tipo', $cliente->pessoa_tipo) }}">
                            <option value="">--Escolha uma opção--</option>
                            <option value="FISICA" {{ ( $cliente->pessoa_tipo == 'FISICA') ? 'selected' : '' }}>Física</option>
                            <option value="JURIDICA" {{ ( $cliente->pessoa_tipo == 'JURIDICA') ? 'selected' : '' }}>Jurídica</option>
                        </select>
                        @error('pessoa_tipo')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col">
                        <label class="form-label" for="documento">{{ __('Documento: ') }}</label>
                        <input class="form-control @error('razao_social') is-invalid @enderror" type="text" id="documento" name="documento" value="{{ old('documento', $cliente->documento) }}">
                        @error('documento')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col">
                        <label class="form-label" for="status">{{ __('Status: ') }}</label>
                        <select class="form-select @error('razao_social') is-invalid @enderror" id="status" name="status" value="{{ old('status', $cliente->status) }}">
                            <option value="">--Escolha uma opção--</option>
                            <option value="ATIVO" {{ ( $cliente->status == 'ATIVO') ? 'selected' : '' }}>Ativo</option>
                            <option value="INATIVO" {{ ( $cliente->status == 'INATIVO') ? 'selected' : '' }}>Inativo</option>
                            <option value="PROSPECTO" {{ ( $cliente->status == 'PROSPECTO') ? 'selected' : '' }}>Prospecto</option>
                        </select>
                        @error('status')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
            <br>
            <div>
                <h3>Endereço</h3>
                <div class="row">

                    <div class="col-8">
                        <label class="form-label" for="rua">{{ __('Rua: ') }}</label>
                        <input class="form-control @error('razao_social') is-invalid @enderror" type="text" id="rua" name="rua" value="{{ old('rua', $cliente?->endereco?->rua) }}">
                        @error('rua')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-4">
                        <label class="form-label" for="bairro">{{ __('Bairro: ') }}</label>
                        <input class="form-control @error('razao_social') is-invalid @enderror" type="text" id="bairro" name="bairro" value="{{ old('bairro', $cliente?->endereco?->bairro) }}">
                        @error('bairro')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-2">
                        <label class="form-label" for="numero">{{ __('Nº: ') }}</label>
                        <input class="form-control @error('razao_social') is-invalid @enderror" type="text" id="numero" name="numero" value="{{ old('numero', $cliente?->endereco?->numero) }}">
                        @error('numero')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-5">
                        <label class="form-label" for="cidade">{{ __('Cidade: ') }}</label>
                        <input class="form-control @error('razao_social') is-invalid @enderror" type="text" id="cidade" name="cidade" value="{{ old('cidade', $cliente?->endereco?->cidade) }}">
                        @error('cidade')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-5">
                        <label class="form-label" for="estado">{{ __('Estado: ') }}</label>
                        <input class="form-control @error('razao_social') is-invalid @enderror" type="text" id="estado" name="estado" value="{{ old('estado', $cliente?->endereco?->estado) }}">
                        @error('cidade')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div>
                        <label class="form-label" for="complemento">{{ __('Complemento: ') }}</label>
                        <input class="form-control @error('razao_social') is-invalid @enderror" type="text" id="complemento" name="complemento" value="{{ old('complemento', $cliente->complemento) }}">
                        @error('complemento')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                </div>
            </div>
            <br>
            <div>
                <h3>Contato</h3>
                <div>
                    <label class="form-label" for="nome">{{ __('Nome: ') }}</label>
                    <input class="form-control @error('razao_social') is-invalid @enderror" type="text" id="nome" name="nome" value="{{ old('nome', $cliente?->contato?->nome) }}">
                    @error('nome')
                    <span class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div>
                    <label class="form-label" for="telefone">{{ __('Telefone: ') }}</label>
                    <input class="form-control @error('razao_social') is-invalid @enderror" type="text" id="telefone" name="telefone" value="{{ old('telefone', $cliente?->contato?->telefone) }}">
                    @error('telefone')
                    <span class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div>
                    <label class="form-label" for="email">{{ __('Email: ') }}</label>
                    <input class="form-control @error('razao_social') is-invalid @enderror" type="text" id="email" name="email" value="{{ old('email', $cliente?->contato?->email) }}">
                    @error('email')
                    <span class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div>
                    <label class="form-label" for="funcao">{{ __('Função: ') }}</label>
                    <input class="form-control @error('razao_social') is-invalid @enderror" type="text" id="funcao" name="funcao" value="{{ old('funcao', $cliente?->contato?->funcao) }}">
                    @error('funcao')
                    <span class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <br>
            <div class="footer">
                <div>
                    <button class="btn btn-light" type=" submit">Salvar</button>
                </div>
                <div>
                    <a class="btn btn-light" href="{{ route('cliente.index') }}" type="submit">Voltar</a>
                </div>
            </div>
        </form>
    </div>
</body>

</html>