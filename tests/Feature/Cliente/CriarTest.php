<?php

namespace Tests\Feature\Cliente;

use App\Models\Cliente;
use App\Models\Contato;
use App\Models\Endereco;
use Tests\TestCase;

class CriarTest extends TestCase
{
    private const ROTA = 'cliente.store';

    public function testFalhaValoresGrandes()
    {
        $valoresGrandes = str_pad('', 300, 'A');

        $novosDados     = [
            'razao_social'  => $valoresGrandes,
            'nome_fantasia' => $valoresGrandes,
            'pessoa_tipo'   => 'FISICO',
            'documento'     => '000.000.000-00',
            'status'        => 'ATIVO',
            'rua'           => $valoresGrandes,
            'estado'        => $valoresGrandes,
            'cidade'        => $valoresGrandes,
            'bairro'        => $valoresGrandes,
            'numero'        => $valoresGrandes,
            'complemento'   => $valoresGrandes,
            'nome'          => $valoresGrandes,
            'telefone'      => '(38)99999-9999',
            'email'         => $valoresGrandes,
            'funcao'        => $valoresGrandes,
        ];

        $response = $this->postJson(route(self::ROTA), $novosDados);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'razao_social',
                    'nome_fantasia',
                    'rua',
                    'estado',
                    'cidade',
                    'bairro',
                    'numero',
                    'complemento',
                    'nome',
                    'email',
                    'funcao',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $novosDados     = [
            'razao_social'  => null,
            'nome_fantasia' => null,
            'pessoa_tipo'   => null,
            'documento'     => null,
            'status'        => null,
            'rua'           => null,
            'estado'        => null,
            'cidade'        => null,
            'bairro'        => null,
            'numero'        => null,
            'nome'          => null,
            'telefone'      => null,
            'email'         => null,
            'funcao'        => null,
        ];

        $response = $this->postJson(route(self::ROTA), $novosDados);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'razao_social',
                    'nome_fantasia',
                    'pessoa_tipo',
                    'documento',
                    'status',
                    'rua',
                    'estado',
                    'cidade',
                    'bairro',
                    'numero',
                    'nome',
                    'telefone',
                    'email',
                    'funcao',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $valorInt = 2;

        $novosDados = [
            'razao_social'  => $valorInt,
            'nome_fantasia' => $valorInt,
            'pessoa_tipo'   => $valorInt,
            'documento'     => $valorInt,
            'status'        => $valorInt,
            'rua'           => $valorInt,
            'estado'        => $valorInt,
            'cidade'        => $valorInt,
            'bairro'        => $valorInt,
            'numero'        => $valorInt,
            'complemento'   => $valorInt,
            'nome'          => $valorInt,
            'telefone'      => $valorInt,
            'email'         => $valorInt,
            'funcao'        => $valorInt,
        ];

        $response = $this->postJson(route(self::ROTA), $novosDados);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'razao_social',
                    'nome_fantasia',
                    'pessoa_tipo',
                    'documento',
                    'status',
                    'rua',
                    'estado',
                    'cidade',
                    'bairro',
                    'numero',
                    'complemento',
                    'nome',
                    'telefone',
                    'email',
                    'funcao',
                ],
            ]);
    }

    public function testFalhaDocumentoJaUtilizado()
    {
        $dadosCliente = Cliente::factory()->create()->toArray();
        $dadosContato = Contato::factory()->create(['cliente_id' => $dadosCliente['id']])->toArray();
        $dadosEndereco = Endereco::factory()->create(['cliente_id' => $dadosCliente['id']])->toArray();
        $novosDados = array_merge(
            $dadosCliente,
            $dadosContato,
            $dadosEndereco
        );

        $response = $this->postJson(route(self::ROTA), $novosDados);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'documento',
                ],
            ]);
    }

    public function testSucesso()
    {
        $dadosCliente = Cliente::factory()->make()->toArray();
        $dadosContato = Contato::factory()->make(['cliente_id' => 0])->toArray();
        $dadosEndereco = Endereco::factory()->make(['cliente_id' => 0])->toArray();
        $novosDados = array_merge(
            $dadosCliente,
            $dadosContato,
            $dadosEndereco
        );

        $response = $this->postJson(route(self::ROTA), $novosDados);
        $response->assertRedirect(route('cliente.index'));

        $this->assertDatabaseHas('clientes', $dadosCliente);
    }
}
