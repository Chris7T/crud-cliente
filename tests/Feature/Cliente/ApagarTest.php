<?php

namespace Tests\Feature\Cliente;

use App\Models\Cliente;
use App\Models\Contato;
use App\Models\Endereco;
use Tests\TestCase;

class ApagarTest extends TestCase
{
    private const ROTA = 'cliente.destroy';
    private const ID_INVALID = '0';

    public function setUp(): void
    {
        parent::setUp();
        $this->cliente = Cliente::factory()->create();
        Contato::factory()->create(['cliente_id' => $this->cliente->getKey()])->toArray();
        Endereco::factory()->create(['cliente_id' => $this->cliente->getKey()])->toArray();
    }


    public function testFalhaClienteNaoEncontrado()
    {
        $response = $this->deleteJson(route(self::ROTA, self::ID_INVALID));

        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $response = $this->deleteJson(route(self::ROTA, $this->cliente->getKey()));

        $this->assertDatabaseMissing('clientes', [
            'razao_social'  => $this->cliente['razao_social'],
            'nome_fantasia' => $this->cliente['nome_fantasia'],
            'pessoa_tipo'   => $this->cliente['pessoa_tipo'],
            'documento'     => $this->cliente['documento'],
            'status'        => $this->cliente['status']
        ]);

        $response->assertRedirect(route('cliente.index'));
    }
}
