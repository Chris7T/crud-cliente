<?php

namespace App\Http\Requests\Contato;

use Illuminate\Foundation\Http\FormRequest;

class ContatoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function bodyParameters(): array
    {
        return [
            'nome'          => [
                'description' => 'Nome do Cliente.',
                'example'     => 'Hamilton Lopes'
            ],
            'email'         => [
                'description' => 'Email do Cliente.',
                'example'     => 'hamilton.lopes@gmail.com'
            ],
            'telefone'      => [
                'description' => 'Telefone do Cliente.',
                'example'     => '(38)99999-9999'
            ],
            'funcao'        => [
                'description' => 'Função do Cliente.',
                'example'     => 'Programador'
            ],
            'cliente_id'    => [
                'description' => 'ID do Cliente.',
                'example'     => 1
            ]
        ];
    }
}
