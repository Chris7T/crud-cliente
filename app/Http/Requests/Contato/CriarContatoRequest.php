<?php

namespace App\Http\Requests\Contato;

class CriarContatoRequest extends ContatoRequest
{
    public function rules()
    {
        return [
            'nome'          => ['filled', 'string', 'name'],
            'email'         => ['filled', 'string', 'email'],
            'funcao'        => ['filled', 'string'],
            'telefone'      => ['filled', 'string', 'celular_com_ddd'],
            'cliente_id'    => ['filled', 'integer', 'exists:clientes,id']
        ];
    }
}
