<?php

namespace App\Http\Requests\Contato;

class CriarContatoRequest extends ContatoRequest
{
    public function rules()
    {
        return [
            'nome'          => ['required', 'string', 'name'],
            'email'         => ['required', 'string', 'email'],
            'telefone'      => ['required', 'string', 'celular_com_ddd'],
            'funcao'        => ['required', 'string'],
            'cliente_id'    => ['required', 'integer', 'exists:clientes,id']
        ];
    }
}
