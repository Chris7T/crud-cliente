<?php

namespace App\Http\Requests\Cliente;

use Illuminate\Foundation\Http\FormRequest;

class ClienteRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function bodyParameters(): array
    {
        return [
            'razao_social'  => [
                'description' => 'Razão Social do Cliente.',
                'example'     => 'Hamilton Lopes'
            ],
            'nome_fantasia' => [
                'description' => 'Nome Fantasia do Cliente.',
                'example'     => 'Hamilton Lopes'
            ],
            'pessoa_tipo'   => [
                'description' => 'Tipo de Pessoa do Cliente.',
                'example'     => 'Física'
            ],
            'documento'     => [
                'description' => 'Documento do Cliente.',
                'example'     => '000.000.000-00'
            ],
            'status'        => [
                'description' => 'Status do Cliente.',
                'example'     => 'Ativo'
            ],
            'estado'        => [
                'description' => 'UF do Cliente.',
                'example'     => 'Minas Gerais'
            ],
            'cidade'        => [
                'description' => 'Cidade do Cliente.',
                'example'     => 'Montes Claros'
            ],
            'rua'           => [
                'description' => 'Rua do Cliente.',
                'example'     => 'Rua 3'
            ],
            'bairro'        => [
                'description' => 'Bairro do Cliente.',
                'example'     => 'Centro'
            ],
            'numero'        => [
                'description' => 'Numero do Cliente.',
                'example'     => '7'
            ],
            'complemento'   => [
                'description' => 'Complemento do Cliente.',
                'example'     => 'Paulo Breno'
            ],
            'nome'          => [
                'description' => 'Nome do Cliente.',
                'example'     => 'Hamilton Lopes'
            ],
            'email'         => [
                'description' => 'Email do Cliente.',
                'example'     => 'hamilton.lopes@gmail.com'
            ],
            'telefone'      => [
                'description' => 'Telefone do Cliente.',
                'example'     => '(38)99999-9999'
            ],
            'funcao'        => [
                'description' => 'Função do Cliente.',
                'example'     => 'Programador'
            ],
            'cliente_id'    => [
                'description' => 'ID do Cliente.',
                'example'     => 1
            ]
        ];
    }
}
