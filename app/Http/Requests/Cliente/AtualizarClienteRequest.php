<?php

namespace App\Http\Requests\Cliente;

use App\Rules\StatusRule;
use App\Rules\TipoClienteRule;
use App\Rules\UniqueRule;

class AtualizarClienteRequest extends ClienteRequest
{
    private const URL_SEGMENTO_PRODUTO_ID = 2;

    public function rules()
    {
        return [
            'razao_social'  => ['required', 'string', 'max:100'],
            'nome_fantasia' => ['required', 'string', 'max:100'],
            'pessoa_tipo'   => ['required', 'string', new TipoClienteRule],
            'documento'     => ['required', 'string', 'formato_cpf_ou_cnpj', new UniqueRule($this->segment(self::URL_SEGMENTO_PRODUTO_ID))],
            'status'        => ['required', 'string', new StatusRule],
            'estado'        => ['required', 'string', 'max:100'],
            'cidade'        => ['required', 'string', 'max:100'],
            'rua'           => ['required', 'string', 'max:100'],
            'bairro'        => ['required', 'string', 'max:100'],
            'numero'        => ['required', 'string', 'max:100'],
            'complemento'   => ['nullable', 'string', 'max:100'],
            'telefone'      => ['required', 'string', 'celular_com_ddd'],
            'nome'          => ['required', 'string', 'max:100'],
            'email'         => ['required', 'string', 'email', 'max:100'],
            'funcao'        => ['required', 'string', 'max:100'],
        ];
    }
}
