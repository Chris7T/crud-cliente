<?php

namespace App\Http\Requests\Cliente;

use App\Rules\StatusRule;
use App\Rules\TipoClienteRule;

class CriarClienteRequest extends ClienteRequest
{
    public function rules()
    {
        return [
            'razao_social'  => ['required', 'string', 'max:100'],
            'nome_fantasia' => ['required', 'string', 'max:100'],
            'pessoa_tipo'   => ['required', 'string', new TipoClienteRule],
            'documento'     => ['required', 'string', 'formato_cpf_ou_cnpj', 'unique:clientes,documento'],
            'status'        => ['required', 'string', new StatusRule],
            'rua'           => ['required', 'string', 'max:100'],
            'estado'        => ['required', 'string', 'max:100'],
            'cidade'        => ['required', 'string', 'max:100'],
            'bairro'        => ['required', 'string', 'max:100'],
            'numero'        => ['required', 'string', 'max:100'],
            'complemento'   => ['nullable', 'string', 'max:100'],
            'nome'          => ['required', 'string', 'max:100'],
            'telefone'      => ['required', 'string', 'celular_com_ddd'],
            'email'         => ['required', 'string', 'email', 'max:100'],
            'funcao'        => ['required', 'string', 'max:100'],
        ];
    }
}
