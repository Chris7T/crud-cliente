<?php

namespace App\Http\Controllers;

use App\Http\Requests\Cliente\CriarClienteRequest as CriarRequest;
use App\Http\Requests\Cliente\AtualizarClienteRequest as AtualizarRequest;
use App\Models\Cliente;

class ClienteController extends Controller
{
    public function index()
    {
        return view('table', ['clientes' => Cliente::all()]);
    }

    public function create()
    {
        return view('form', ['cliente' => new Cliente(), 'action' => 'cliente.store', 'method' => 'POST']);
    }

    public function store(CriarRequest $request)
    {
        $cliente = Cliente::create($request->validated());
        $cliente->contato()->updateOrCreate(['cliente_id' => $cliente->getKey()], $request->validated());
        $cliente->endereco()->updateOrCreate(['cliente_id' => $cliente->getKey()], $request->validated());

        return redirect()->route('cliente.index');
    }

    public function edit(Cliente $cliente)
    {
        return view('form', ['cliente' => $cliente, 'action' => 'cliente.update', 'method' => 'PUT']);
    }

    public function update(AtualizarRequest $request, Cliente $cliente)
    {
        $cliente->update($request->validated());
        $cliente->contato()->updateOrCreate(['cliente_id' => $cliente->getKey()], $request->validated());
        $cliente->endereco()->updateOrCreate(['cliente_id' => $cliente->getKey()], $request->validated());

        return redirect()->route('cliente.index');
    }

    public function destroy(Cliente $cliente)
    {
        $cliente->delete();
        return redirect()->route('cliente.index');
    }
}
