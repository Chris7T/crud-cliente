<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class StatusRule implements Rule
{
    public function passes($attribute, $value)
    {
        return (in_array($value, ['ATIVO', 'INATIVO', 'PROSPECTO']));
    }

    public function message()
    {
        return 'The client status is invalid.';
    }
}
