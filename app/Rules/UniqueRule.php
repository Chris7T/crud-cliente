<?php

namespace App\Rules;

use App\Models\Cliente;
use Illuminate\Contracts\Validation\Rule;

class UniqueRule implements Rule
{

    public function __construct(int $cliente_id)
    {
        $this->cliente = Cliente::findOrFail($cliente_id);
    }

    public function passes($attribute, $value)
    {
        if ($this->cliente->documento == $value) {
            return true;
        } else {
            $count = Cliente::where('documento', $value)->count();
            return ($count == 0);
        }
    }

    public function message()
    {
        return 'The documento is invalid.';
    }
}
