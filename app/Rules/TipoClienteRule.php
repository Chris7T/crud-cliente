<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class TipoClienteRule implements Rule
{
    public function passes($attribute, $value)
    {
        return (in_array($value, ['FISICA', 'JURIDICA']));
    }

    public function message()
    {
        return 'The tipo de cliente status is invalid.';
    }
}
