<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Endereco extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $table      = 'enderecos';
    protected $fillable   = [
        'estado',
        'cidade',
        'rua',
        'bairro',
        'numero',
        'complemento',
        'cliente_id'
    ];

    public function cliente(): BelongsTo
    {
        return $this->belongsTo(Cliente::class, 'cliente_id', 'id');
    }
}
