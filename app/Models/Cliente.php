<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Cliente extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $table      = 'clientes';
    protected $fillable   = [
        'razao_social',
        'nome_fantasia',
        'pessoa_tipo',
        'documento',
        'status',
    ];

    public function contato(): HasOne
    {
        return $this->hasOne(Contato::class, 'cliente_id', 'id');
    }

    public function endereco(): HasOne
    {
        return $this->hasOne(Endereco::class, 'cliente_id', 'id');
    }
}
